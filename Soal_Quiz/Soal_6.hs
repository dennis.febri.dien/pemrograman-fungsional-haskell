phytagoras :: (a -> b -> c)
phytagoras = [(x,y,z) | z <- [5..] , y <- [z, z-1..1], x <- [y,y-1..1], z^2 == (y^2 + x^2) ]