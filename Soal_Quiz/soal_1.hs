kpk :: Integer -> Integer -> Integer
kpk x y = minList [i | i <-(map (*x) [1..y]), j <-(map (*y) [1..x]), i==j]
minList (x:xs) = foldl (min) x xs