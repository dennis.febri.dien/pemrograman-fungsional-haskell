checking :: String -> Bool
checking "" = False
checking (c:cs)
  | c `elem` "0123456789" = True
  | otherwise = checking cs


checkNumber :: [String] -> [String]
checkNumber [] = []
checkNumber (x:xs)
  | checking x = [] ++ checkNumber xs
  | otherwise = [x] ++ checkNumber xs
  
checkNumVowel :: [String] -> [String]
checkNumVowel [] = []
checkNumVowel (x:xs)
  | ((counting x) >= 4) = [] ++ checkNumVowel xs
  | otherwise = [x] ++ checkNumVowel xs
  
checkRepetitive :: [String] -> [String]
checkRepetitive [] = []
checkRepetitive (x:xs)
  | (isRepetitive x) = [] ++ checkRepetitive xs
  | otherwise = [x] ++ checkRepetitive xs

  
counting :: String -> Integer
counting "" = 0
counting xs = foldr (+) 0 (isVowels xs)

isVowels :: String -> [Integer]
isVowels "" = []
isVowels (x:xs)
  | x `elem` "aiueo" = [1] ++ (isVowels xs)
  | otherwise = [0] ++ (isVowels xs)
  
isRepetitive :: String -> Bool
isRepetitive (x:xs)
  | xs == "" = False
  | x == head(xs) = True
  | otherwise = isRepetitive xs
  
weirdFilter :: [String] -> [String]
weirdFilter [] = []
weirdFilter xs = checkRepetitive(checkNumVowel(checkNumber xs))