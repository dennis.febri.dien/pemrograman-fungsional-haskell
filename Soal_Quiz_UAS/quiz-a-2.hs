rotabc :: String -> String
rotabc "" = ""
rotabc (x:xs)
  | x == 'a' = "b" ++ (rotabc xs)
  | x == 'b' = "c" ++ (rotabc xs)
  | x == 'c' = "a" ++ (rotabc xs)
  | otherwise = [x] ++ (rotabc xs)