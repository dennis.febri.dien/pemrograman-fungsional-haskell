import qualified Data.Char as Char
import Data.List.Split
import Data.List

capitalized :: String -> String
capitalized (head:tail) = Char.toUpper head : map Char.toLower tail
capitalized [] = []


-- Example how to use
-- return [my,comma,...]
main :: IO ()
main = print $
  intercalate " " (map capitalized (splitOn "," "my,comma,separated,list"))

checkString :: String -> String -> [(String, String)]  
checkString sW lsX = [ (y,x) | x<-(inp1 lsX), y<-(inp1 sW) ]
  where inp1 s = splitOn " " s
  
cap :: [String] -> [String] -> [String]
cap [] items = []
cap (s:ls) items
  | s `elem` items = [s] ++ (cap ls items)
  | otherwise = [capitalized s] ++ (cap ls items)

capitalize :: String -> [String] -> String
capitalize strinput lsexcept = intercalate " " (cap (splitOn " " strinput) lsexcept)