compose :: (b -> c) -> (a -> b) -> a -> c
compose y z = \x -> y (z x)