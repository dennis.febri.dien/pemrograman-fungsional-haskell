data Tree a = Leaf a
    | Node a (Tree a) (Tree a)
    deriving Show

treeToList :: Tree a -> [a]
treeToList (Leaf a) = [a]
treeToList (Node a (b) (c))  = treeToList(b) ++ [a] ++ treeToList(c)