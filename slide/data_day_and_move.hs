data Day = Sun | Mon | Tue | Wed | Thu | Fri | Sat
   deriving Show
valday :: Integer -> Day
valday 1 = Sun
valday 2 = Mon
valday 3 = Tue
valday 4 = Wed
valday 5 = Thu
valday 6 = Fri
valday 7 = Sat

dayval :: Day -> Integer
dayval Sun = 1 
dayval Mon = 2 
dayval Tue = 3 
dayval Wed = 4 
dayval Thu = 5 
dayval Fri = 6 
dayval Sat = 7 

data Move = Paper | Rock | Scrissor
    deriving Show

beats :: Move -> Move
beats Paper = Rock
beats Rock = Scrissor
beats Scrissor = Paper