import Data.List

add [] []         = []
add (a:as) (b:bs) = (a+b) : (add as bs)

fibs  =  1 : 1 : add fibs (tail fibs)

-- permutation :: [a] -> [[a]]
permutation [] = [[]]
permutation a = [ x:ps | x<-a, ps <-permutation (a\\[x])]

